//
//  Assets.swift
//  Space Delivery
//
//  Created by Ali Çelik on 5.05.2021.
//

import UIKit

extension UIImage {
    static let favoriteImage = UIImage(named: "favorite")
    static let unfavoriteImage = UIImage(named: "unfavorite")
}
