//
//  FavoriteStations.swift
//  Space Delivery
//
//  Created by Ali Çelik on 5.05.2021.
//

import Foundation

private let listKey = "FavoriteStations"

extension UserDefaults {

    func changeFavoriteStatus(of name: String) -> Bool {
        guard var list = array(forKey: listKey) as? [String] else {
            let newList = [name]
            setValue(newList, forKey: listKey)
            return true
        }
        if let existIndex = list.firstIndex(where: { $0 == name }) {
            list.remove(at: existIndex)
            setValue(list, forKey: listKey)
            return false
        } else {
            list.append(name)
            setValue(list, forKey: listKey)
            return true
        }
    }

    func getFavoriteStatus(of name: String) -> Bool {
        guard let list = array(forKey: listKey) as? [String] else {
            return false
        }
        return list.contains(name)
    }
}
