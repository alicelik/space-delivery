//
//  BorderView.swift
//  Space Delivery
//
//  Created by Ali Çelik on 5.05.2021.
//

import UIKit

// these classes were created to fix the issue of borderColor changing on appearance change
class BorderView: UIView {
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        setAppBorderColor()
    }
}

class BorderButton: UIButton {
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        setAppBorderColor()
    }
}

class BorderTabBar: UITabBar {
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        setAppBorderColor()
    }
}

class BorderSearchBar: UISearchBar {
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        setAppBorderColor()
    }
}

