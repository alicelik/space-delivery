//
//  ShipConstructorViewModel.swift
//  DailyExercise
//
//  Created by Ali Çelik on 1.05.2021.
//

import Foundation

protocol ViewModelProtocol: AnyObject {
}

public class ShipConstructorViewModel: ViewModelProtocol {

    let shipModel = ShipModel.shared

    var isCompleted: Bool {
        shipModel.totalPoint == 15
    }

    var pointsToBeDistributel: Int { 15 - shipModel.totalPoint }
}

