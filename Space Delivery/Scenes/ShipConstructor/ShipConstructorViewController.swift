//
//  ShipConstructorViewController.swift
//  DailyExercise
//
//  Created by Ali Çelik on 1.05.2021.
//

import UIKit

final class ShipConstructorViewController: UIViewController {

    @IBOutlet var pointsToBeDistributedLabel: UILabel!
    @IBOutlet var nameTextField: UITextField!

    @IBOutlet var eusSlider: UISlider!
    @IBOutlet var eusLabel: UILabel!

    @IBOutlet var dsSlider: UISlider!
    @IBOutlet var dsLabel: UILabel!

    @IBOutlet var ugsSlider: UISlider!
    @IBOutlet var ugsLabel: UILabel!

    let viewModel = ShipConstructorViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setRemainingPoints()
    }

    @IBAction func touchUpInside(_ sender: UISlider) {
        sender.value = round(sender.value)
        if ugsSlider == sender {
            setValue(
                viewModel.shipModel.setUgs(sender.value),
                label: ugsLabel,
                slider: sender)
        } else if eusSlider == sender {
            setValue(
                viewModel.shipModel.setEus(sender.value),
                label: eusLabel,
                slider: sender)
        } else /** dsSlider */ {
            setValue(
                viewModel.shipModel.setDs(sender.value),
                label: dsLabel,
                slider: sender)
        }

        setRemainingPoints()
    }

    func setRemainingPoints() {
        pointsToBeDistributedLabel.text = viewModel.pointsToBeDistributel.string
    }

    func setValue(_ value: Float, label: UILabel, slider: UISlider) {
        UIView.animate(withDuration: 0.25, animations: {
            slider.value = value
            label.text = String(value.int)
        })
    }

    @IBAction func continuePressed(_ sender: Any) {
        if !viewModel.isCompleted { return }

        viewModel.shipModel.name = nameTextField.text ?? ""
        let vc = MainContainerViewController()
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }

}
