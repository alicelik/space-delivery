//
//  MainContainerViewController.swift
//  DailyExercise
//
//  Created by Ali Çelik on 2.05.2021.
//

import UIKit

class MainContainerViewController: UIViewController {

    @IBOutlet var tabBar: UITabBar!
    @IBOutlet var contentView: UIView!

    let viewModel = MainContainerViewModel()

    lazy var mainVC = MainViewController()
    lazy var favoriteVC = FavoriteViewController()

    var currentIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        tabBar.selectedItem = tabBar.items?.first
        viewModel.requestStations { [weak self] in
            DispatchQueue.main.async {
                self?.setStations()
            }
        }
    }

    func setStations() {
        mainVC.setStations(viewModel.stations)
        favoriteVC.setStations(viewModel.stations)
        updateContent(with: mainVC)
    }

    func updateContent(with vc: UIViewController) {
        contentView.removeSubviews()
        removeChildren()

        contentView.addSubview(vc.view)
        vc.view.equalToSuperview()
        addChild(vc)
    }

}

extension MainContainerViewController: UITabBarDelegate {

    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag == currentIndex { return }
        currentIndex = item.tag
        switch item.tag {
        case 0:
            updateContent(with: mainVC)
        case 1:
            updateContent(with: favoriteVC)
        default:
            break
        }
    }

}
