//
//  MainViewModel.swift
//  Space Delivery
//
//  Created by Ali Çelik on 3.05.2021.
//

import Foundation

enum MainViewAlertText: String {
    case travelFinished = "Delivery is completed"
    case noNeed = "Station no need"
    case failed = "Coudn't be delivred"
    case won = "You've won"
    case lost = "You've lost"
    case reasonEus = "Not enough time"
    case reasonDamage = "Not enough damage"
    case reasonUgs = "Not enough ugs"
}

enum TravelState {
    case travelFinished
    case noNeed
    case lost(message: MainViewAlertText)
    case won
}

protocol MainViewStateDelegate: AnyObject {
    func travelDidEnd(_ state: TravelState)
    func alert(_ message: MainViewAlertText)
}

class MainViewModel {

    weak var stateDelegate: MainViewStateDelegate?

    var currentIndex = 0
    var stations: StationModel = StationModel()
    var completedStations = StationModel()
    var currentStation: StationModelItem?
    var missionCompleted: Bool { completedStations.count == stations.count }

    func moveToStation(at index: Int) {
        guard let from = currentStation, stations.isIndexInRange(index) else { return }
        let to = stations[index]
        guard to.need != 0 else {
            if missionCompleted {
                stateDelegate?.travelDidEnd(.won)
            } else {
                stateDelegate?.alert(.noNeed)
            }
            return
        }

        let distance = abs(from.coordinateX - to.coordinateX) + abs(from.coordinateY - to.coordinateY)
        let travel = Travel(distance: distance, ugs: to.need)
        if let errorType = ShipModel.shared.travel(travel) {
            throwErrorWithType(errorType)
            return
        }
        to.need = 0
        to.stock = to.capacity
        currentStation = to
        completedStations.append(to)
        succeedHandler()
    }

    func succeedHandler() {
        if missionCompleted {
            stateDelegate?.travelDidEnd(.won)
        } else if let errorType = ShipModel.shared.outOfResources {
            throwErrorWithType(errorType)
        } else {
            stateDelegate?.travelDidEnd(.travelFinished)
        }
    }

    func throwErrorWithType(_ type: ShipModel.ErrorResourceType) {
        switch type {
        case .ugs:
            stateDelegate?.travelDidEnd(.lost(message: .reasonUgs))
        case .eus:
            stateDelegate?.travelDidEnd(.lost(message: .reasonEus))
        case .damage:
            stateDelegate?.travelDidEnd(.lost(message: .reasonDamage))
        }
    }

    func itemFor(searchText: String) -> Bool {
        let upperCasedSearchText = searchText.uppercased(with: Locale(identifier: "tr"))
        var i = 0
        var containingIndex: Int?
        let index: Int? = stations.firstIndex { item in
            if containingIndex == nil {
                if item.name.uppercased().contains(upperCasedSearchText) {
                    containingIndex = i
                } else {
                    i += 1
                }
            }

            return item.name.uppercased().starts(with: upperCasedSearchText)
        }
        
        if let index = index {
            currentIndex = index
            return true
        } else if let containingIndex = containingIndex {
            currentIndex = containingIndex
            return true
        }

        return false
    }

}

