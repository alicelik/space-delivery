//
//  StationCollectionViewCell.swift
//  Space Delivery
//
//  Created by Ali Çelik on 3.05.2021.
//

import UIKit

enum StationCellState {
    case travel
    case left
    case right
}

class StationCollectionViewCell: UICollectionViewCell {

    @IBOutlet var leftFirstLabel: UILabel!
    @IBOutlet var leftSecondLabel: UILabel!
    @IBOutlet var centerLabel: UILabel!
    @IBOutlet var favoriteButton: UIButton!
    
    var stateHandler: ((StationCellState) -> Void)?

    var station: StationModelItem! {
        didSet {
            setData()
        }
    }
    var isFav = false
    var favImage: UIImage? { isFav ? .favoriteImage: .unfavoriteImage }

    func setData() {
        guard let station = station else { return }
        leftFirstLabel.text = "\(station.capacity)/\(station.stock)"
        leftSecondLabel.text = String(station.need)
        centerLabel.text = String(station.name)
        isFav = UserDefaults.standard.getFavoriteStatus(of: station.name)
        favoriteButton.setImage(favImage, for: .normal)
    }

    @IBAction func travelPressed(_ sender: Any) {
        stateHandler?(.travel)
    }

    @IBAction func leftPressed(_ sender: Any) {
        stateHandler?(.left)
    }

    @IBAction func rightPressed(_ sender: Any) {
        stateHandler?(.right)
    }

    @IBAction func favoriteButtonPressed(_ sender: Any) {
        isFav = UserDefaults.standard.changeFavoriteStatus(of: station.name)
        favoriteButton.setImage(favImage, for: .normal)
    }

}
