//
//  MainViewController.swift
//  DailyExercise
//
//  Created by Ali Çelik on 2.05.2021.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet var ugsLabel: UILabel!
    @IBOutlet var eusLabel: UILabel!
    @IBOutlet var dsLabel: UILabel!

    @IBOutlet var shipNameLabel: UILabel!
    @IBOutlet var damageLabel: UILabel!
    @IBOutlet var unnamedLabel: UILabel!

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var bottomLabel: UILabel!

    @IBOutlet var collectionView: UICollectionView!

    let viewModel = MainViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.delegate = self
        viewModel.stateDelegate = self
        collectionView.register(forType: StationCollectionViewCell.self)
        refreshUIData()
    }

    func setStations(_ stations: StationModel) {
        viewModel.currentStation = stations.first
        viewModel.stations = stations
        viewModel.completedStations.append(stations.first!)
    }

    func refreshUIData() {
        ugsLabel.text = "UGS: " + ShipModel.shared.ugs.string
        eusLabel.text = "EUS: " + ShipModel.shared.eus.string
        dsLabel.text = "DS: " + ShipModel.shared.ds.string
        shipNameLabel.text = ShipModel.shared.name
        damageLabel.text = ShipModel.shared.damage.string
        bottomLabel.text = viewModel.currentStation?.name
    }

    func refreshUIWithSucceedStatus() {
        refreshUIData()
        collectionView.reloadItems(at: [IndexPath(row: viewModel.currentIndex, section: 0)])
    }

    func cardViewHandler(_ state: StationCellState) {
        switch state {
        case .travel:
            viewModel.moveToStation(at: viewModel.currentIndex)
        case .left:
            if viewModel.currentIndex == 0 { return }
            viewModel.currentIndex -= 1
            scrollToCurrentIndex()
        case .right:
            if viewModel.currentIndex == viewModel.stations.lastIndex { return }
            viewModel.currentIndex += 1
            scrollToCurrentIndex()
        }
    }

    func scrollToCurrentIndex() {
        collectionView.scrollToItem(
            at: IndexPath(row: viewModel.currentIndex, section: 0),
            at: .centeredHorizontally,
            animated: true)
    }

}

extension MainViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        guard let text = searchBar.text else {
            return
        }
        if viewModel.itemFor(searchText: text) {
            scrollToCurrentIndex()
        }
        searchBar.text = nil
    }

}

extension MainViewController: MainViewStateDelegate {
    
    func alert(_ message: MainViewAlertText) {
        showStandartAlert(message: message.rawValue)
    }

    func travelDidEnd(_ state: TravelState) {
        switch state {
        case .travelFinished:
            alert(.travelFinished)
            refreshUIWithSucceedStatus()
        case .noNeed:
            alert(.noNeed)
        case .won:
            alert(.won)
            unnamedLabel.text = MainViewAlertText.won.rawValue
            refreshUIWithSucceedStatus()
        case .lost(let message):
            let lostText = MainViewAlertText.lost.rawValue
            showStandartAlert(title: lostText,
                              message: message.rawValue)
            unnamedLabel.text = lostText
        }
    }

}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.stations.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeue(forType: StationCollectionViewCell.self, at: indexPath) else {
            return .init()
        }
        cell.station = viewModel.stations[indexPath.row]
        cell.stateHandler = cardViewHandler
        return cell
    }

}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.frame.width,
               height: collectionView.frame.width)
    }
}
