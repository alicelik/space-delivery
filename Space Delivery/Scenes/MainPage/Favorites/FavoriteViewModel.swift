//
//  FavoriteViewModel.swift
//  Space Delivery
//
//  Created by Ali Çelik on 5.05.2021.
//

import Foundation

class FavoriteViewModel {

    var favoriteStations = StationModel()
    var allStations = StationModel()

    func refreshFavoriteList() {
        favoriteStations = allStations.filter({ item in
            UserDefaults.standard.getFavoriteStatus(of: item.name)
        })
    }
}
