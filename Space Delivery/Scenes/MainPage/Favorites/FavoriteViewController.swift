//
//  FavoriteViewController.swift
//  DailyExercise
//
//  Created by Ali Çelik on 2.05.2021.
//

import UIKit

class FavoriteViewController: UIViewController {

    @IBOutlet var tableView: UITableView!

    let viewModel = FavoriteViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(forType: FavoriteTableViewCell.self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        viewModel.refreshFavoriteList()
        tableView.reloadData()
    }

    func setStations(_ stations: StationModel) {
        viewModel.allStations = stations
    }

}

extension FavoriteViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeue(forType: FavoriteTableViewCell.self, at: indexPath) else {
            return .init()
        }
        cell.station = viewModel.favoriteStations[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.favoriteStations.count
    }

}
