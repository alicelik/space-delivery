//
//  FavoriteTableViewCell.swift
//  Space Delivery
//
//  Created by Ali Çelik on 5.05.2021.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {

    @IBOutlet var stationNameLabel: UILabel!
    @IBOutlet var stationEusLabel: UILabel!
    @IBOutlet var favoriteButton: UIButton!

    var station: StationModelItem! {
        didSet {
            setData()
        }
    }
    var isFav = false
    var favImage: UIImage? { isFav ? .favoriteImage: .unfavoriteImage }

    func setData() {
        guard let station = station else { return }
        isFav = UserDefaults.standard.getFavoriteStatus(of: station.name)
        stationNameLabel.text = station.name
        stationEusLabel.text = "?"
        favoriteButton.setImage(favImage, for: .normal)
    }

    @IBAction func favoritePressed(_ sender: Any) {
        isFav = UserDefaults.standard.changeFavoriteStatus(of: station.name)
        favoriteButton.setImage(favImage, for: .normal)
    }

}
