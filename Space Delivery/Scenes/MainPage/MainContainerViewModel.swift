//
//  MainContainerViewModel.swift
//  DailyExercise
//
//  Created by Ali Çelik on 2.05.2021.
//

import Foundation

class MainContainerViewModel: NSObject {

    var stations = StationModel()

    func requestStations(_ completion: @escaping () -> Void) {
        URLSession.shared.request(with: MockyEndpoint.getStationList) { [weak self] (stations: StationModel) in
            guard let self = self else { return }
            self.stations = stations
            completion()
        }
    }

}
