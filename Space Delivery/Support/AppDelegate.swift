//
//  AppDelegate.swift
//  Space Delivery
//
//  Created by Ali Çelik on 3.05.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = .init()
        window?.rootViewController = ShipConstructorViewController()
        window?.makeKeyAndVisible()
        return true
    }

}

