//
//  UIView+Subviews.swift
//  Space Delivery
//
//  Created by Ali Çelik on 3.05.2021.
//

import UIKit

extension UIView {

    func equalToSuperview() {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        leadingAnchor.constraint(equalTo: superview.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: superview.trailingAnchor).isActive = true
        topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
    }

    func removeSubviews() {
        subviews.forEach { view in
            view.removeFromSuperview()
        }
    }

}
