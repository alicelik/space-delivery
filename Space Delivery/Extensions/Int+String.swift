//
//  Int+String.swift
//  Space Delivery
//
//  Created by Ali Çelik on 3.05.2021.
//

import Foundation

extension Int {

    var string: String {
        String(self)
    }

}
