//
//  Array+LastIndex.swift
//  Space Delivery
//
//  Created by Ali Çelik on 4.05.2021.
//

import Foundation

extension Array {

    var lastIndex: Int {
        count - 1
    }

    func isIndexInRange(_ index: Int) -> Bool {
        index >= 0  && lastIndex >= index
    }
}
