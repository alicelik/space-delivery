//
//  Alert.swift
//  Space Delivery
//
//  Created by Ali Çelik on 4.05.2021.
//

import UIKit

extension UIViewController {

    func showStandartAlert(title: String? = nil, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            alertController.dismiss(animated: true, completion: nil)
        }))
        present(alertController, animated: true, completion: nil)
    }
}
