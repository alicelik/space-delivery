//
//  Int+Float.swift
//  Space Delivery
//
//  Created by Ali Çelik on 3.05.2021.
//

import Foundation

extension Float {
    var int: Int {
        Int(self)
    }
}

extension Int {
    var float: Float {
        Float(self)
    }
}
