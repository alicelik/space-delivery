//
//  UITableViiew+Register.swift
//  Space Delivery
//
//  Created by Ali Çelik on 5.05.2021.
//

import UIKit

extension UITableView {
    
    func register<T: UITableViewCell>(forType: T.Type) {
        let className = String(describing: T.self)
        let nib = UINib(nibName: className, bundle: Bundle(for: T.self))
        register(nib, forCellReuseIdentifier: className)
    }
    
    func dequeue<T: UITableViewCell>(forType type: T.Type, at indexPath: IndexPath) -> T? {
        dequeueReusableCell(withIdentifier: String(describing: T.self), for: indexPath) as? T
    }
    
}

