//
//  URLSession+getRequest.swift
//  Space Delivery
//
//  Created by Ali Çelik on 3.05.2021.
//

import Foundation

protocol Request {
    var urlRequest: URLRequest? { get }
}

extension URLSession {

    func request<T: Decodable>(with request: Request, successHandler: @escaping ((T) -> Void), _ errorHandler: ((Error?) -> Void)? = nil) {
        guard let urlRequest = request.urlRequest else {
            errorHandler?(nil)
            return
        }
        dataTask(with: urlRequest) { data, response, error in
            guard let data = data else {
                errorHandler?(nil)
                return
            }
            let response: T
            do {
                response = try JSONDecoder().decode(T.self, from: data)
            } catch {
                errorHandler?(error)
                return
            }
             
            successHandler(response)
        }.resume()
    }

}
