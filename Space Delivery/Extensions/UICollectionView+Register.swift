//
//  UICollectionView+Register.swift
//  Space Delivery
//
//  Created by Ali Çelik on 4.05.2021.
//

import UIKit

extension UICollectionView {
    
    func register<T: UICollectionViewCell>(forType: T.Type) {
        let className = String(describing: T.self)
        let nib = UINib(nibName: className, bundle: Bundle(for: T.self))
        register(nib, forCellWithReuseIdentifier: className)
    }
    
    func dequeue<T: UICollectionViewCell>(forType type: T.Type, at indexPath: IndexPath) -> T? {
        dequeueReusableCell(withReuseIdentifier: String(describing: T.self), for: indexPath) as? T
    }
    
}

