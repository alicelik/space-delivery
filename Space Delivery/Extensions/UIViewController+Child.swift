//
//  UIViewController+Child.swift
//  Space Delivery
//
//  Created by Ali Çelik on 3.05.2021.
//

import UIKit

extension UIViewController {

    func removeChildren() {
        children.forEach { child in
            child.removeFromParent()
        }
    }
}
