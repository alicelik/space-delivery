//
//  UIView+Border.swift
//  Space Delivery
//
//  Created by Ali Çelik on 3.05.2021.
//

import UIKit

@IBDesignable
extension UIView {

    @IBInspectable var appBorder: Bool {
        set {
            layer.masksToBounds = true
            layer.borderWidth = 2
            setAppBorderColor()
        }
        get {
            layer.masksToBounds
        }
    }

    func setAppBorderColor() {
        if #available(iOS 13.0, *) {
            layer.borderColor = UIColor.label.cgColor
        } else {
            layer.borderColor = UIColor.black.cgColor
        }
    }
}
