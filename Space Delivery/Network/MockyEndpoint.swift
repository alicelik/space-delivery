//
//  Endpoint.swift
//  Space Delivery
//
//  Created by Ali Çelik on 3.05.2021.
//

import Foundation

private let mockyGetList = "https://run.mocky.io/v3/e7211664-cbb6-4357-9c9d-f12bf8bab2e2"

enum MockyEndpoint: Request {
    case getStationList

    var urlRequest: URLRequest? {
        let url: URL?
        switch self {
        case .getStationList:
            url = URL(string: mockyGetList)
        }
        if let url = url {
            return URLRequest(url: url)
        }
        return nil
    }
}
