import Foundation

// MARK: - StationModelElement
public class StationModelItem: Decodable {
    let name: String
    let coordinateX, coordinateY, capacity: Int
    var stock, need: Int
}

public typealias StationModel = [StationModelItem]
