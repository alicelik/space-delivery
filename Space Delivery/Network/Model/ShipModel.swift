import Foundation

struct Travel {
    var distance: Int
    var ugs: Int
    var eus: Int { distance }
}

public class ShipModel: NSObject {

    enum ErrorResourceType {
        case ugs
        case eus
        case damage
    }

    public static let shared = ShipModel()

    public private(set) var ugs: Int = 10000
    public private(set) var eus: Int = 20
    public private(set) var ds: Int = 10000
    public var name: String = ""

    private var ugsRaw: Int = 1 {
        didSet {
            ugs = 10000 * ugsRaw
        }
    }

    private var eusRaw: Int = 1 {
        didSet {
            eus = 20 * eusRaw
        }
    }

    private var dsRaw: Int = 1 {
        didSet {
            ds = 10000 * dsRaw
        }
    }

    private var remainingDamageTime: Int = 0
    var damage: Int = 100
    var outOfResources: ErrorResourceType? {
        if ugs == 0 { return .ugs }
        else if eus == 0 { return .eus }
        else if damage == 0 { return .damage }
        return nil
    }

    public var totalPoint: Int { ugsRaw + eusRaw + dsRaw }
    public lazy var damagePeriod = TimeInterval(ds / 1000)

    public override init() { }

    func setUgs(_ rawValue: Float) -> Float {
        self.ugsRaw = returnAvailableValue(rawValue.int, values: [eusRaw, dsRaw])
        return self.ugsRaw.float
    }

    func setEus(_ rawValue: Float) -> Float {
        self.eusRaw = returnAvailableValue(rawValue.int, values: [ugsRaw, dsRaw])
        return self.eusRaw.float
    }

    func setDs(_ rawValue: Float) -> Float {
        self.dsRaw = returnAvailableValue(rawValue.int, values: [ugsRaw, eusRaw])
        return self.dsRaw.float
    }

    func returnAvailableValue(_ rawValue: Int, values: [Int]) -> Int {
        if (values + [rawValue]).reduce(0, +) > 15 {
            return 15 - values.reduce(0, +)
        }
        return rawValue
    }

    func travel(_ travel: Travel) -> ErrorResourceType? {
        let damageEus = travel.eus + remainingDamageTime
        let mod = damageEus % (ds/1000)
        let damageToDecrease = ((damageEus - mod) / (ds/1000)) * 10
        if travel.eus > eus {
            return .eus
        } else if travel.ugs > ugs {
            return .ugs
        } else if damageToDecrease > damage {
            return .damage
        }
        eus -= travel.eus
        damage -= damageToDecrease
        ugs -= travel.ugs
        remainingDamageTime = mod
        return nil
    }

}
